module Example exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Grid
import List.Extra
import Test exposing (..)


suite : Test
suite =
    describe "the Grid module"
        [ describe "Grid.empty"
            [ fuzz (Fuzz.intRange 1 40) "an empty grid has the right number of cells" <|
                \i ->
                    gridOfSize i
                        |> Grid.contents
                        |> List.length
                        |> Expect.equal (i * i)
            ]
        , describe "Grid.sideLength"
            [ fuzz (Fuzz.intRange 1 40) "returns the right value" <|
                \i ->
                    gridOfSize i
                        |> Grid.sideLength
                        |> Expect.equal i
            ]
        , describe "Grid.set"
            [ test "sets the right cell" <|
                \_ ->
                    gridOfSize 5
                        |> Grid.set ( 2, 1 ) (Grid.Text "TARGET")
                        |> cellAtIndex 11
                        |> Expect.equal (Grid.Text "TARGET")
            ]
        , describe "Grid.symmetricSet"
            [ test "sets the right cells" <|
                \_ ->
                    gridOfSize 5
                        |> Grid.symmetricSet ( 2, 1 ) (Grid.Text "TARGET")
                        |> Expect.all
                            [ \grid -> grid |> cellAtIndex 11 |> Expect.equal (Grid.Text "TARGET")
                            , \grid -> grid |> cellAtIndex 8 |> Expect.equal (Grid.Text "TARGET")
                            ]
            ]
        ]


gridOfSize n =
    Grid.empty n


cellAtIndex n grid =
    grid
        |> Grid.contents
        |> List.Extra.getAt 11
        |> Maybe.withDefault Grid.Blocked
