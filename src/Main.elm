module Main exposing (main)

import Browser
import Grid exposing (Cell(..), ClueHook(..), Grid)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import List.Extra exposing (..)


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , subscriptions = subscriptions
        , update = update
        , view = view
        }


init : () -> ( Model, Cmd Msg )
init _ =
    ( emptyModel 15, Cmd.none )


type Requestable s
    = Requested s


type Clue
    = Empty Int
    | Clue Int String


type alias Model =
    { wordBank : Requestable (List String)
    , grid : Grid
    , clues :
        { vertical : List Clue
        , horizontal : List Clue
        }
    }


emptyModel : Int -> Model
emptyModel gridHeight =
    let
        grid =
            Grid.empty gridHeight

        clueHooks =
            grid |> Grid.clueHooks
    in
    { wordBank = Requested []
    , grid = grid
    , clues =
        { vertical = clueHooks |> filterForVertical |> hookToClue
        , horizontal = clueHooks |> filterForHorizontal |> hookToClue
        }
    }


hookToClue : List ClueHook -> List Clue
hookToClue hooks =
    hooks
        |> List.concatMap
            (\hook ->
                case hook of
                    Horizontal i ->
                        [ Empty i ]

                    Vertical i ->
                        [ Empty i ]

                    Both i ->
                        [ Empty i ]

                    NoHook ->
                        []
            )


filterForHorizontal : List ClueHook -> List ClueHook
filterForHorizontal clues =
    List.filter
        (\hook ->
            case hook of
                Both _ ->
                    True

                Horizontal _ ->
                    True

                _ ->
                    False
        )
        clues


filterForVertical : List ClueHook -> List ClueHook
filterForVertical clues =
    List.filter
        (\hook ->
            case hook of
                Both _ ->
                    True

                Vertical _ ->
                    True

                _ ->
                    False
        )
        clues


type Msg
    = SetCell Grid.Coord String
    | Block Grid.Coord
    | Unblock Grid.Coord
    | SetGridSize (Maybe Int)


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


update : Msg -> Model -> ( Model, Cmd Msg )
update m model =
    case m of
        Block c ->
            let
                newGrid =
                    Grid.symmetricSet c Blocked model.grid
            in
            ( { model
                | grid = newGrid
                , clues =
                    { vertical = Grid.clueHooks newGrid |> filterForVertical |> hookToClue
                    , horizontal = Grid.clueHooks newGrid |> filterForHorizontal |> hookToClue
                    }
              }
            , Cmd.none
            )

        Unblock c ->
            let
                newGrid =
                    Grid.symmetricSet c (Text "") model.grid
            in
            ( { model
                | grid = newGrid
                , clues =
                    { vertical = Grid.clueHooks newGrid |> filterForVertical |> hookToClue
                    , horizontal = Grid.clueHooks newGrid |> filterForHorizontal |> hookToClue
                    }
              }
            , Cmd.none
            )

        SetCell c v ->
            ( { model | grid = Grid.set c (Text v) model.grid }, Cmd.none )

        SetGridSize n ->
            case n of
                Nothing ->
                    ( model, Cmd.none )

                Just i ->
                    -- Grids should be between 5 and 30 characters wide.
                    ( emptyModel (Basics.min (Basics.max i 5) 30), Cmd.none )


view : Model -> Html Msg
view model =
    div [ style "display" "flex" ]
        [ section [ style "flex-grow" "1" ]
            [ div []
                [ text "grid size:"
                , input
                    [ onInput (SetGridSize << String.toInt)
                    , value (String.fromInt (Grid.sideLength model.grid))
                    ]
                    []
                ]
            , table
                [ style "font-style" "monospace"
                , style "font-size" baseFontSize
                , style "border" "0.05em solid black"
                , style "border-collapse" "collapse"
                ]
                (groupsOf
                    (Grid.sideLength model.grid)
                    (zip (Grid.contents model.grid) (Grid.clueHooks model.grid))
                    |> List.indexedMap makeRow
                )
            ]
        , section [ style "flex-grow" "1" ]
            [ h1 [] [ text "Horizontal" ]
            , ol []
                (model.clues.horizontal
                    |> makeClueLis
                )
            ]
        , section [ style "flex-grow" "1" ]
            [ h1 [] [ text "Vertical" ]
            , ol []
                (model.clues.vertical
                    |> makeClueLis
                )
            ]
        ]


baseFontSize : String
baseFontSize =
    "16pt"


makeRow : Int -> List ( Cell, ClueHook ) -> Html Msg
makeRow rowIndex columns =
    tr
        [ style "display" "block"
        , style "line-height" "0em"
        ]
        (List.indexedMap (makeCell rowIndex) columns)


makeCell : Int -> Int -> ( Cell, ClueHook ) -> Html Msg
makeCell rowIndex colIndex ( cell, clueHook ) =
    case cell of
        Blocked ->
            cellTd
                [ style "background-color" "black"
                , onDoubleClick (Unblock ( rowIndex, colIndex ))
                ]
                [ clueNumber [] [] ]

        Text s ->
            cellTd
                [ onDoubleClick (Block ( rowIndex, colIndex ))
                ]
                [ case clueHook of
                    NoHook ->
                        text ""

                    Horizontal i ->
                        clueNumber [] [ text (String.fromInt (i + 1)) ]

                    Vertical i ->
                        clueNumber [] [ text (String.fromInt (i + 1)) ]

                    Both i ->
                        clueNumber [] [ text (String.fromInt (i + 1)) ]
                , cellInput
                    [ style "background-color"
                        (if s == "" then
                            "rgb(255,255,224)"

                         else
                            "white"
                        )
                    , value s
                    , onInput
                        (String.toLower
                            >> SetCell ( rowIndex, colIndex )
                        )
                    ]
                    []
                ]


cellTd : List (Attribute Msg) -> List (Html Msg) -> Html Msg
cellTd attrs children =
    td
        ([ style "border" "0.05em solid black"
         , style "display" "inline-block"
         , style "padding" "0"
         , style "width" "1.6em"
         , style "height" "1.6em"
         , style "overflow" "hidden"
         ]
            ++ attrs
        )
        children


cellInput : List (Attribute Msg) -> List (Html Msg) -> Html Msg
cellInput attrs children =
    input
        ([ style "font-size" baseFontSize
         , style "border" "0"
         , style "font-family" "monospace"
         , style "text-align" "center"
         , style "text-transform" "uppercase"
         , style "vertical-align" ""
         , style "width" "100%"
         , style "height" "100%"
         ]
            ++ attrs
        )
        children


clueNumber : List (Attribute Msg) -> List (Html Msg) -> Html Msg
clueNumber attrs children =
    Html.div
        ([ style "font-size" "6pt"
         , style "position" "relative"
         , style "vertical-align" "top"
         , style "width" "0"
         , style "height" "0"
         ]
            ++ attrs
        )
        [ Html.div [ style "position" "absolute", style "top" "1em", style "left" "0.5em" ] children ]


makeClueLis : List Clue -> List (Html Msg)
makeClueLis clues =
    List.map
        (\clue ->
            case clue of
                Empty i ->
                    li [ value (String.fromInt (i + 1)) ] [ input [] [] ]

                Clue i t ->
                    li [ value (String.fromInt (i + 1)) ] [ input [ value t ] [] ]
        )
        clues
