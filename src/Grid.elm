module Grid exposing
    ( Cell(..)
    , ClueHook(..)
    , Coord
    , Grid
    , clueHooks
    , contents
    , empty
    , set
    , sideLength
    , symmetricSet
    )

import List.Extra exposing (groupsOf, setAt, transpose, zip)


{-| The interface to the Grid data structure.

This includes:

  - The Grid type itself
  - Ways to extract information from the Grid
  - Ways to transform one Grid into another Grid

-}
type Cell
    = Blocked
    | Text String


type ClueHook
    = NoHook
    | Horizontal Int
    | Vertical Int
    | Both Int


type alias Coord =
    ( Int, Int )


type Grid
    = Grid Int (List Cell)


empty : Int -> Grid
empty sl =
    let
        seed =
            List.repeat (sl * sl) (Text "")
    in
    Grid sl
        seed


sideLength : Grid -> Int
sideLength (Grid i _) =
    i


contents : Grid -> List Cell
contents (Grid _ cells) =
    cells


clueHooks : Grid -> List ClueHook
clueHooks (Grid sl cells) =
    findClueSites sl cells


set : Coord -> Cell -> Grid -> Grid
set ( r, c ) cell (Grid sl cells) =
    let
        offset =
            r * sl + c

        newCells =
            setAt offset cell cells
    in
    Grid sl newCells


symmetricSet : Coord -> Cell -> Grid -> Grid
symmetricSet ( r, c ) cell (Grid sl cells) =
    let
        offset =
            r * sl + c

        newContents =
            cells
                |> setAt offset cell
                |> setAt (sl * sl - offset - 1) cell
    in
    Grid sl newContents


combinedClueSites : List ( ClueHook, ClueHook ) -> List ClueHook
combinedClueSites zippedClues =
    combinedClueSitesRecursive 0 zippedClues


combinedClueSitesRecursive : Int -> List ( ClueHook, ClueHook ) -> List ClueHook
combinedClueSitesRecursive count remainingHooks =
    case remainingHooks of
        [] ->
            []

        ( Horizontal _, Vertical _ ) :: rest ->
            Both count :: combinedClueSitesRecursive (count + 1) rest

        ( Horizontal _, NoHook ) :: rest ->
            Horizontal count :: combinedClueSitesRecursive (count + 1) rest

        ( NoHook, Vertical _ ) :: rest ->
            Vertical count :: combinedClueSitesRecursive (count + 1) rest

        ( _, _ ) :: rest ->
            NoHook :: combinedClueSitesRecursive count rest


findClueSites : Int -> List Cell -> List ClueHook
findClueSites sl cells =
    let
        rows =
            groupsOf sl cells

        horizontalClueHooks =
            (rows
                |> List.map findHorizontalClueSites
            )
                |> List.concat

        verticalClueHooks =
            transpose
                (transpose rows
                    |> List.map findVerticalClueSites
                )
                |> List.concat
    in
    combinedClueSites (zip horizontalClueHooks verticalClueHooks)


findHorizontalClueSites : List Cell -> List ClueHook
findHorizontalClueSites cells =
    findClueSitesRecursive Horizontal [] 0 Blocked cells


findVerticalClueSites : List Cell -> List ClueHook
findVerticalClueSites cells =
    findClueSitesRecursive Vertical [] 0 Blocked cells


findClueSitesRecursive : (Int -> ClueHook) -> List ClueHook -> Int -> Cell -> List Cell -> List ClueHook
findClueSitesRecursive clueType hooks clueCount lastCell restCells =
    let
        next3 =
            List.take 3 restCells

        rest =
            List.drop 3 restCells
    in
    if List.length next3 < 3 then
        hooks ++ List.repeat (List.length next3) NoHook

    else
        case next3 of
            [ Text _, Text _, Text a ] ->
                case lastCell of
                    Blocked ->
                        findClueSitesRecursive
                            clueType
                            (hooks ++ [ clueType clueCount, NoHook, NoHook ])
                            (clueCount + 1)
                            (Text a)
                            rest

                    Text _ ->
                        findClueSitesRecursive
                            clueType
                            (hooks ++ [ NoHook, NoHook, NoHook ])
                            clueCount
                            (Text a)
                            rest

            Blocked :: c ->
                findClueSitesRecursive
                    clueType
                    (hooks ++ [ NoHook ])
                    clueCount
                    Blocked
                    (c ++ rest)

            (Text a) :: c ->
                findClueSitesRecursive
                    clueType
                    (hooks ++ [ NoHook ])
                    clueCount
                    (Text a)
                    (c ++ rest)

            [] ->
                hooks
